package br.com.scm.ste.app.aluno;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

public class Aluno implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String nome;
	private Date nascimento;
	
	Aluno(){
	}
	
	private Aluno(String nome, Date nascimento){
		this.nome = nome;
		this.nascimento = nascimento;
	}
	
	public static Aluno newInstance(String nome, Date nascimento){
		checkArgument(Strings.isNullOrEmpty(nome));
		checkNotNull(nascimento);
		checkArgument(new Date().compareTo(nascimento) > 0 , "Data de nascimento deve ser maior que a data de atual.");
		return new Aluno(nome, nascimento);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Aluno){
			Aluno other = (Aluno) obj;
			return Objects.equal(this.nome, other.nome) && 
					Objects.equal(this.nascimento, other.nascimento);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(nome, nascimento);
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public Date getNascimento() {
		return nascimento;
	}

	
	
	

}
