package br.com.scm.ste.app.endereco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.scm.ste.app.endereco.Cidade;
import br.com.scm.ste.app.repository.ListQueryDslPredicateExecutor;

public interface CidadeRepository extends JpaRepository<Cidade, Long>, ListQueryDslPredicateExecutor<Cidade>{

}
