package br.com.scm.ste.app.endereco;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import com.google.common.base.Objects;

import br.com.insula.opes.Cep;
import br.com.scm.ste.app.endereco.estado.Estado;

@Entity
public class Logradouro implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="cidade_fk")
	private Cidade cidade;
	
	private String logradouro;
	
	private String bairro;
	
	@Type(type="cep")
	private Cep cep;
	
	private String tipoLogradouro;
	
	Logradouro(){
		
	}
	
	public Logradouro(Ac ac) {
		this.cidade = Cidade.newInstance(ac.getCidade(), Estado.BA);
		this.logradouro = ac.getLogradouro();
		this.bairro = ac.getBairro();
		this.cep = Cep.fromString(ac.getCep());
		this.tipoLogradouro = ac.getTipoLogradouro();
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id)
				.add("cidade", cidade)
				.add("logradouro", logradouro)
				.add("bairro", bairro)
				.add("cep", cep)
				.add("tpLog", tipoLogradouro).toString();
	}
	
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

}
