package br.com.scm.ste.app.endereco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.scm.ste.app.endereco.Ac;
import br.com.scm.ste.app.repository.ListQueryDslPredicateExecutor;

public interface AcRepository extends JpaRepository<Ac, Long>, ListQueryDslPredicateExecutor<Ac>{

}
