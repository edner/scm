package br.com.scm.ste.app.login.seguranca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.scm.ste.app.login.seguranca.Usuario;
import br.com.scm.ste.app.repository.ListQueryDslPredicateExecutor;

public interface UsuarioRepository extends JpaRepository<Usuario , Long>, ListQueryDslPredicateExecutor<Usuario> {

}
