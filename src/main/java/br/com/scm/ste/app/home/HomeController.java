package br.com.scm.ste.app.home;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.mysema.query.BooleanBuilder;

import br.com.scm.ste.app.endereco.Ac;
import br.com.scm.ste.app.endereco.Cidade;
import br.com.scm.ste.app.endereco.Logradouro;
import br.com.scm.ste.app.endereco.QCidade;
import br.com.scm.ste.app.endereco.estado.Estado;
import br.com.scm.ste.app.endereco.repository.AcRepository;
import br.com.scm.ste.app.endereco.repository.CidadeRepository;
import br.com.scm.ste.app.endereco.repository.LogradouroRepository;


@Controller
public class HomeController {
	
	@Autowired
	AcRepository acRepository;
	
	@Autowired
	LogradouroRepository logradouroRepository;
	
	@Autowired
	CidadeRepository cidadeRepository;
	
	public void rodar(){
		List<Ac> lista = acRepository.findAll();
		
		for(Ac ac : lista){
			Logradouro l = new Logradouro(ac);
			BooleanBuilder builder = new BooleanBuilder();
			builder.and(QCidade.cidade.nome.eq(ac.getCidade()));
			builder.and(QCidade.cidade.estado.eq(Estado.BA));
			Cidade cidade = cidadeRepository.findOne(builder);
			if(cidade != null){
				l.setCidade(cidade);
			}
			logradouroRepository.save(l);
		}
		
		System.out.println("terminou");
	}
	
}
