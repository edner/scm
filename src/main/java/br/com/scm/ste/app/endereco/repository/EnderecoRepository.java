package br.com.scm.ste.app.endereco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.scm.ste.app.endereco.Endereco;
import br.com.scm.ste.app.repository.ListQueryDslPredicateExecutor;

public interface EnderecoRepository extends JpaRepository<Endereco, Long>, ListQueryDslPredicateExecutor<Endereco> {

}
