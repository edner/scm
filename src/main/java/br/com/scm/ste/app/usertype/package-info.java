@TypeDefs({ @TypeDef(name = "cep", defaultForType = Cep.class, typeClass = CepUserType.class) })
package br.com.scm.ste.app.usertype;

import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.insula.opes.Cep;
import br.com.insula.opes.hibernate.usertype.CepUserType;

