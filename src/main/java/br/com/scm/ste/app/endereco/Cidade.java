package br.com.scm.ste.app.endereco;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

import br.com.scm.ste.app.endereco.estado.Estado;

import com.google.common.base.Objects;

@Entity
public class Cidade implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;
	
	private String nome;
	
	@Type(type="estado")
	private Estado estado;
	
	Cidade(){
	}
	
	private Cidade(String nome, Estado estado){
		this.nome = nome;
		this.estado = estado;
		
	}
	
	public static Cidade newInstance(String nome, Estado estado){
		checkNotNull(nome);
		checkNotNull(estado);
		return new Cidade(nome, estado);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cidade){
			Cidade other = (Cidade) obj;
			return Objects.equal(this.nome, other.nome) &&
					Objects.equal(this.estado, other.estado);
		}
		return false;
		
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hashCode(nome, estado);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("nome", nome).add("estado", estado).toString();
	}
	
	public Long getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Estado getEstado() {
		return estado;
	}

}
